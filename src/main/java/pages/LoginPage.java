package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import bl.framework.design.projectMethods;

public class LoginPage extends projectMethods{
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this); 
	}
@FindBy(how=How.ID,using="email") WebElement eleUsername;
@FindBy(how=How.ID,using="password") WebElement elePwd;
@FindBy(how=How.ID,using="buttonLogin") WebElement eleLogin;

	public LoginPage enterUsername(String data)
	{
	clearAndType(eleUsername, data);
	return this;
	}
	public LoginPage enterPassword(String data)
	{
		clearAndType(elePwd, data);
		return this;
	}
	public HomePage clickLogin()
	{
		click(eleLogin);
		return new HomePage();
	}
}
