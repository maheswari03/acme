package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import bl.framework.design.projectMethods;

public class PrintFirstResultPage extends projectMethods{
	public PrintFirstResultPage()
	{
		PageFactory.initElements(driver, this); 
	}
	@FindBy(how=How.XPATH,using="//td[text()='Maxitronic Limited']") WebElement eleFirstResult;
	
	public PrintFirstResultPage findFirstResult()
	{
		System.out.println(getElementText(eleFirstResult));
		return this;
		
	}

}
