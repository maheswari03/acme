package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import bl.framework.design.projectMethods;

public class VendorSearch extends projectMethods {
	
	public VendorSearch()
	{
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID,using="vendorTaxID") WebElement eleVendorID;
	@FindBy(how=How.ID,using="buttonSearch") WebElement eleClickSearch;
	
	public VendorSearch searchVendor(String data)
	{
		clearAndType(eleVendorID, data);
		return this;
	}
	public PrintFirstResultPage clickSearch()
	{
		click(eleClickSearch);
		return new PrintFirstResultPage();
		
	}
	

}
