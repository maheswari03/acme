package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import bl.framework.design.projectMethods;

public class HomePage extends projectMethods {
	Actions builder = new Actions(driver);
	public HomePage()
	{
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.XPATH,using="(//button[@class='btn btn-default btn-lg'])[4]") WebElement eleMouseOverVendor;
	@FindBy(how= How.LINK_TEXT, using="Search for Vendor") WebElement eleClickSearch;
	
	public HomePage mouseOverVendor()
	{
		
		builder.moveToElement(eleMouseOverVendor).pause(3000).perform();
		return this;
	}
	public VendorSearch clickSearchVendor()
	{
		builder.moveToElement(eleClickSearch).click().perform();
		return new VendorSearch();
		
	}
}
