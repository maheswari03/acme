package bl.framework.design;


import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;
import bl.framework.utils.*;
public class projectMethods extends SeleniumBase {

	public String testCaseName, testDescription, testNodes, author, category, dataSheetName;
	@BeforeSuite
	public void beforeSuite() {
		startReport();
	}
	@AfterSuite
	public void afterSuite() {
		endReport();
	}
	@BeforeClass
	public void beforeClass(){		
		startTestModule(testCaseName, testDescription);	
	}
	
	@BeforeMethod
	public void beforeMethod(){
		test = startTestCase(testNodes);
		test.assignAuthor(author);
		test.assignCategory(category);
		startApp("chrome", "https://acme-test.uipath.com/account/login");		
	}
	
	@AfterMethod
	public void closeApp() {
		close();
	}
	
	@DataProvider(name="fetchData")
	public Object[][] getData() {
		return DataInputProvider.readData(dataSheetName);
	}

}
