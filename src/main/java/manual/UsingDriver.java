package manual;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class UsingDriver {
	@Test
	public void driver() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("maheswarid03@gmail.com");
		driver.findElementById("password").sendKeys("Mahes@1994");
		driver.findElementById("buttonLogin").click();
		Actions builder=new Actions(driver);
		WebElement ele = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
		builder.moveToElement(ele).pause(3000).perform();
		builder.moveToElement(driver.findElementByLinkText("Search for Vendor")).click().perform();
		driver.findElementById("vendorTaxID").sendKeys("DE767565");
		driver.findElementById("buttonSearch").click();
		/*WebElement table = driver.findElementByClassName("table");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		WebElement firstrow = rows.get(1);
		String text = firstrow.findElement(By.tagName("td")).getText();
		System.out.println(text);*/
		
		String text = driver.findElementByXPath("//td[text()='Maxitronic Limited']").getText();
		System.out.println(text);
		
	}

}
