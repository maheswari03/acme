package testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.design.projectMethods;
import pages.LoginPage;

public class UsingPOM extends projectMethods {
	
	@BeforeTest
	public void setdata()
	{
		testCaseName="ACME";
		testDescription = "Search for a vendor and display the Result";
		testNodes ="Vendor";
		author = "Mahes";
		category ="Smoke";
		dataSheetName="ACME";
				
	}
	@Test(dataProvider="fetchData")
	public void acme(String username, String password, String vendorID)
	{
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.mouseOverVendor()
		.clickSearchVendor()
		.searchVendor(vendorID)
		.clickSearch()
		.findFirstResult();
	}
}
